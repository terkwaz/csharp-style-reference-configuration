The reference style configuration for .Net Core projects

## Setup

- Add [StyleCop.Analyzers nuget package version 1.2.0-beta.261](https://www.nuget.org/packages/StyleCop.Analyzers/1.2.0-beta.261) or higher to each project in the solution, adding it to just one shared project won’t work.

- Copy the configuration files from this repo into your solution folder. Place them in the same folder that houses your top-level solution file (.sln).

- Update the each project file (.csproj) by adding the following:

```xml
<PropertyGroup>
  <CodeAnalysisRuleSet>../.ruleset</CodeAnalysisRuleSet>
  <GenerateDocumentationFile>true</GenerateDocumentationFile> <!-- This is essential for enforcing the documentation rules at build time -->
  <TreatWarningsAsErrors>true</TreatWarningsAsErrors> <!-- Only include this for new projects, see migration guide below -->
</PropertyGroup>

<ItemGroup>
  <AdditionalFiles Include="../stylecop.json" />
</ItemGroup>
```

Make sure to update the relative path of the files to match your setup.

- Only incluce `GenerateDocumentationFile` if you're starting from scratch, don't add it to existing p

## Severity Levels

The highest severity level that we'll allow in the config will be `Warning`, once all warnings are resolved we'll be adding a directive to all projects instructing msbuild to treat warnings as errors.

To test individual rules and help filter them out you can temporarily configure one rule to be `Error` instead of `Warning`.

Ruleset files should not be changed individually for each project, the idea is to have a unified codestyle across the company not just in a single project. Any change to the configuration should be approved by the .Net technical team lead.

## Migration Guide

If you have an existing solution, follow these steps:

- You can start with all projects in the solution or do it one project at a time.
- Do not enable the `TreatWarningsAsErrors` property in .csproj. Only enable this after you've treated all warnings.
- Commit and fix all the warnings gradually, you'll likely get a lot of them and some of them cannot be bulk-fixed.
- After fixing all warnings, enable `TreatWarningsAsErrors`, at this point, any rule violation will cause the build to fail in the IDE, console, and the pipelines (if any.)

## Overriding Rules

If you want to override a rule inline, use the following syntax to wrap the code block in question:

```csharp
#pragma warning disable SA1202 // ElementsMustBeOrderedByAccess
    // Code goes here...
#pragma warning restore SA1202 // ElementsMustBeOrderedByAccess
```

Use sparingly, the you'll be required to add an explanation to each override, PR reviewers are encouraged to double down on these and only use them when the override makes a large section of the code more readable.

## Known Issues

- Rider has poor support for updating the ruleset file and it defaults to their internal config files.
- Both Rider and Visual Studio will have trouble recognizing all the rules after the initial setup, a restart will do in most cases, also try rebuilding.
- Rider doesn't provide the option to apply fixes to the entire solution or project for these analyzers while Visual Studio 2019 provides that option for some of them (not all.) It's recommended that you use Visual Studio 2019 for applying the rules to an existing solution.

## Participation

If you want to make changes, create pull requests and let's start a discussion.

- Updates to the config files and the README are both acceptable.
- Do not convert a rule's level to `Error`, the highest level should be warning.
- If you're updating a rule, include the rule Id at the beginning of the PR title.
- Include fixes for one rule at a time in one PR, the only case in which you can change two or more rules in one PR is if the rules are tightly coupled.

## References

Please refer to [StyleCop's detailed documentation](https://github.com/DotNetAnalyzers/StyleCopAnalyzers/tree/master/documentation) for more details.
